import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MainApp());

class MainApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: "SF UI Text"),
      home: Scaffold(
        backgroundColor: Color(0xfff5f4f9),
        body: Container(width: double.infinity, child: FavoritesPage()),
        bottomSheet: BottomNavigationBar(
          showSelectedLabels: false,
          showUnselectedLabels: false,
          unselectedItemColor: Colors.grey[400],
          selectedItemColor: Colors.grey[850],
          elevation: 0,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.explore),
              title: Text('Home'),
              backgroundColor: Color(0xfff5f4f9),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.layers),
              title: Text('Business'),
              backgroundColor: Color(0xfff5f4f9),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.folder),
              backgroundColor: Color(0xfff5f4f9),
              title: Text('School'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              backgroundColor: Color(0xfff5f4f9),
              title: Text('School'),
            ),
          ],
        ),
      ),
    );
  }
}

class CardCity extends StatefulWidget {
  CardCity({
    @required this.image,
    @required this.title,
    @required this.location,
    @required this.price,
  });

  final String image;
  final String title;
  final String location;
  final String price;

  @override
  _CardCityState createState() => _CardCityState();
}

class _CardCityState extends State<CardCity> with TickerProviderStateMixin {
  AnimationController _animController;
  AnimationController _scaleAninControll;

  @override
  void initState() {
    super.initState();
    _animController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 150),
    );
    _scaleAninControll = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 150),
    );

    _scaleAninControll.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _scaleAninControll.reverse();
      } else if (status == AnimationStatus.dismissed) {
        _scaleAninControll.stop();
      }
    });
  }

  @override
  void dispose() {
    _animController.dispose();
    _scaleAninControll.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: FadeTransition(
              opacity: Tween<double>(begin: 0, end: 1).animate(_animController),
              child: Container(
                height: double.infinity,
                width: double.infinity,
                color: Color(0xff170f58),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  alignment: Alignment.centerRight,
                  child: IconButton(
                    onPressed: () => print("DEleting"),
                    icon: Icon(Icons.delete_outline),
                    iconSize: 36,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          AnimatedBuilder(
            animation: _scaleAninControll,
            builder: (context, child) {
              return Transform.scale(
                scale: Tween<double>(
                  begin: 1,
                  end: 0.8,
                ).animate(_scaleAninControll).value,
                child: child,
              );
            },
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: GestureDetector(
                onHorizontalDragUpdate: (dragDetails) {
                  if (dragDetails.primaryDelta < 0) {
                    _animController.forward();
                  } else {
                    _animController.reverse();
                  }
                },
                child: SlideTransition(
                  position: Tween<Offset>(
                    begin: Offset(0, 0),
                    end: Offset(-0.25, 0),
                  ).animate(_animController),
                  child: Container(
                    padding: EdgeInsets.all(14),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Container(
                            height: 70,
                            width: 70,
                            child: Image.network(
                              widget.image,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 6,
                          child: Container(
                            height: 85,
                            padding: EdgeInsets.symmetric(
                                horizontal: 24, vertical: 12),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  widget.title,
                                  style: TextStyle(
                                    fontSize: 18,
                                    letterSpacing: -1,
                                    color: Colors.grey[800],
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Text(
                                  widget.location,
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Color(0xffafafaf),
                                  ),
                                ),
                                Text(
                                  widget.price,
                                  style: TextStyle(
                                    fontSize: 14,
                                    letterSpacing: -1,
                                    color: Color(0xff170f58),
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            alignment: Alignment.center,
                            child: Container(
                              width: 42,
                              height: 42,
                              decoration: BoxDecoration(
                                color: Color(0xff170f58),
                                borderRadius: BorderRadius.circular(12),
                              ),
                              child: IconButton(
                                color: Colors.white,
                                icon: Icon(Icons.arrow_forward),
                                onPressed: () {
                                  _scaleAninControll.forward();

                                  Timer(Duration(milliseconds: 200), () {
                                    Navigator.of(context).push(
                                      _createRoute(
                                        ProfilePage(),
                                      ),
                                    );
                                  });
                                },
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}


class FavoritesPage extends StatelessWidget {
  Widget _singleCard(
    String image,
    String title,
    String location,
    String price,
  ) {
    return CardCity(
      image: image,
      title: title,
      location: location,
      price: price,
    );
  }

  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 48),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 16),
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 8),
                  child: Text(
                    "Favoritos",
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "4 Resultados",
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                    RichText(
                      text: TextSpan(
                        style: TextStyle(
                          color: Color(0xff170f58),
                        ),
                        children: [
                          TextSpan(
                            text: "Ordenar por: ",
                          ),
                          TextSpan(
                            text: "Recientes",
                            style: TextStyle(
                              letterSpacing: -1,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                _singleCard(
                  "https://storage3d.com/storage/2017.01/4a112d96db506eb4bf9065114972189c.jpg",
                  "París",
                  "Seoul",
                  "\$476.000",
                ),
                _singleCard(
                  "https://storage3d.com/storage/2009.04/14c9c2ab299237e2406ee4818bfa8b5e.jpg",
                  "Neighdown",
                  "Teha",
                  "\$186.000",
                ),
                _singleCard(
                  "https://storage3d.com/storage/2018.07/0ca2755180f030dd461d298a7029a5a6.jpg",
                  "Cummerdown",
                  "Sereland",
                  "\$226.000",
                ),
                _singleCard(
                  "https://storage3d.com/storage/2009.10/bd490ef5f7071ad75409658e8cedea7c.jpg",
                  "Sandals",
                  "Mohema",
                  "\$196.000",
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

Route _createRoute(Widget childWidget) {
  return PageRouteBuilder(
    pageBuilder: (BuildContext context, animation, seconAnim) => childWidget,
    transitionsBuilder: (BuildContext context, anim, seconAnin, child) {
      return SlideTransition(
        position:
            Tween<Offset>(begin: Offset(1, 0), end: Offset(0, 0)).animate(anim),
        child: Scaffold(
          body: child,
        ),
      );
      // double widthScreen = MediaQuery.of(context).size.width;
      // return Transform.translate(
      //   offset: Tween<Offset>(begin: Offset(widthScreen, 0), end: Offset(0, 0))
      //       .animate(anim)
      //       .value,
      //   child: Scaffold(
      //     body: child,
      //   ),
      // );
    },
  );
}

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with TickerProviderStateMixin {
  AnimationController _initAnimator;

  @override
  void initState() {
    super.initState();
    _initAnimator = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 1300));
    Timer(Duration(milliseconds: 200), () => _initAnimator.forward());
  }

  @override
  void dispose() {
    _initAnimator.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 300,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(
                    "https://co.habcdn.com/photos/project/big/patio-interior-y-hall-de-habitaciones-primer-piso-181720.jpg",
                  ),
                ),
              ),
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: 28,
                    left: 14,
                    child: IconButton(
                      onPressed: () => Navigator.of(context).pop(),
                      icon: Icon(
                        Icons.keyboard_backspace,
                        size: 26,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Transform.translate(
              offset: Offset(0, -24),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16),
                ),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    // Información de usuario */
                    FadeTransition(
                      opacity: _initAnimator.drive(CurveTween(
                        curve: Interval(0.2, 0.7, curve: Curves.easeInCirc),
                      )),
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              flex: 16,
                              child: CircleAvatar(
                                radius: 30,
                                backgroundColor: Colors.transparent,
                                backgroundImage: NetworkImage(
                                  "https://randomuser.me/api/portraits/men/22.jpg",
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 59,
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 16),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Jack Beneth",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w700,
                                          fontSize: 16,
                                          color: Colors.grey[800]),
                                    ),
                                    Text(
                                      "Propietario",
                                      style: TextStyle(color: Colors.grey[600]),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 25,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    height: 35,
                                    width: 35,
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(36),
                                      color: Colors.grey[200],
                                    ),
                                    child: IconButton(
                                      iconSize: 18,
                                      padding: EdgeInsets.all(0),
                                      color: Colors.grey[400],
                                      icon: Icon(Icons.phone),
                                      onPressed: () =>
                                          print("Pressed the Call"),
                                    ),
                                  ),
                                  Container(
                                    height: 35,
                                    width: 35,
                                    margin: EdgeInsets.only(left: 8),
                                    padding: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(36),
                                      color: Colors.grey[200],
                                    ),
                                    child: IconButton(
                                      iconSize: 18,
                                      padding: EdgeInsets.all(0),
                                      color: Colors.grey[400],
                                      icon: Icon(Icons.chat_bubble),
                                      onPressed: () =>
                                          print("Pressed the Call"),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),

                    // Presupuestos */
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          FadeTransition(
                            opacity: _initAnimator.drive(
                              CurveTween(curve: Curves.ease),
                            ),
                            child: SlideTransition(
                              position: Tween(
                                begin: Offset(-0.6, 0),
                                end: Offset(0, 0),
                              ).animate(
                                CurvedAnimation(
                                  curve: Curves.ease,
                                  parent: _initAnimator,
                                ),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(bottom: 4),
                                    child: Text(
                                      "Presupuesto XX",
                                      style: TextStyle(fontSize: 15),
                                    ),
                                  ),
                                  Text(
                                    "\$2.690.000",
                                    style: TextStyle(
                                      color: Color(0xff170f58),
                                      fontSize: 18,
                                      fontWeight: FontWeight.w700,
                                      letterSpacing: -1,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          FadeTransition(
                            opacity: _initAnimator.drive(
                              CurveTween(curve: Curves.ease),
                            ),
                            child: SlideTransition(
                              position: Tween(
                                begin: Offset(0.6, 0),
                                end: Offset(0, 0),
                              ).animate(
                                CurvedAnimation(
                                  curve: Curves.ease,
                                  parent: _initAnimator,
                                ),
                              ),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(right: 8),
                                    child: Icon(
                                      Icons.widgets,
                                      size: 18,
                                      color: Colors.grey[700],
                                    ),
                                  ),
                                  Container(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          margin: EdgeInsets.only(bottom: 4),
                                          child: Text(
                                            "\$3.962.000/mo",
                                            style: TextStyle(
                                              color: Color(0xff170f58),
                                              fontSize: 18,
                                              fontWeight: FontWeight.w700,
                                              letterSpacing: -1,
                                            ),
                                          ),
                                        ),
                                        Text(
                                          "Ver Capacidad",
                                          style: TextStyle(fontSize: 15),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

                    // Detalles de Sitio */
                    FadeTransition(
                      opacity: Tween<double>(
                        begin: 0,
                        end: 1,
                      ).animate(CurvedAnimation(
                        curve: Interval(0.3, 1, curve: Curves.ease),
                        parent: _initAnimator,
                      )),
                      child: SlideTransition(
                        position: Tween<Offset>(
                          begin: Offset(0, 0.3),
                          end: Offset(0, 0),
                        ).animate(CurvedAnimation(
                          curve: Interval(0.3, 1, curve: Curves.ease),
                          parent: _initAnimator,
                        )),
                        child: Container(
                          margin: EdgeInsets.only(top: 24),
                          height: 160,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                flex: 2,
                                child: Text(
                                  "Detalles de sitio",
                                  style: TextStyle(
                                    fontSize: 16,
                                    height: 1,
                                    color: Colors.grey[800],
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 8,
                                child: Container(
                                  child: GridView.count(
                                    padding: EdgeInsets.all(0),
                                    primary: false,
                                    shrinkWrap: true,
                                    mainAxisSpacing: 12,
                                    crossAxisCount: 3,
                                    childAspectRatio: 2.3,
                                    children: <Widget>[
                                      _singleFieldProperty("Habitaciones", "4"),
                                      _singleFieldProperty("Baños", "2"),
                                      _singleFieldProperty("Área", "680 mt2"),
                                      _singleFieldProperty(
                                          "Antigüedad", "2015"),
                                      _singleFieldProperty("Parqueadero", "No"),
                                      _singleFieldProperty("Sector", "Alta"),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    // Descripciones de sitio */
                    FadeTransition(
                      opacity: Tween<double>(
                        begin: 0,
                        end: 1,
                      ).animate(CurvedAnimation(
                        curve: Interval(0.6, 1, curve: Curves.ease),
                        parent: _initAnimator,
                      )),
                      child: SlideTransition(
                        position: Tween(
                          begin: Offset(0, 0.3),
                          end: Offset(0, 0),
                        ).animate(
                          CurvedAnimation(
                            curve: Interval(0.6, 1, curve: Curves.ease),
                            parent: _initAnimator,
                          ),
                        ),
                        child: Container(
                          margin: EdgeInsetsDirectional.only(top: 24),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Descripciones",
                                style: TextStyle(
                                  fontSize: 16,
                                  height: 1,
                                  color: Colors.grey[800],
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Container(
                                margin: EdgeInsetsDirectional.only(top: 8),
                                child: Text(
                                  "Interactively recaptiualize synergistic e-commerce through clicks-and-mortar architectures. Appropriately cultivate mission-critical e-services rather than functionalized.",
                                  style: TextStyle(color: Colors.grey[600]),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _singleFieldProperty(String key, String val) {
    return Container(
      // padding: EdgeInsets.symmetric(vertical: 8),
      child: Column(
        children: <Widget>[
          Text(
            key,
            style: TextStyle(
              color: Colors.grey[600],
              height: 1,
            ),
          ),
          Text(
            val,
            style: TextStyle(fontWeight: FontWeight.w700),
          ),
        ],
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
    );
  }
}
